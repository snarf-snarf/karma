# karma
<i> A library for automated statistical forecasting and time series machine learning </i> 

- One-line SARIMA with both stochastic and deterministic training options (meant as an improvement to auto.arima)
- One-line Box-Jenkins model constuction with training options for approximation meta-algorithms (via heuristic optimisation)
- Backwards compatibility with package 'forecast' (for Hyndman's ARIMA and neural network autoregression objects)
- Functionality for in-sample and out-of-sample model validation (diagnose overfitting/underfitting via train-test splitting)
- Functionality for training multiple weak learners to make stronger predictions (ensemble learning / stacking)
- Functionality for autoregressive random forest (meta-learning on Breiman's 'randomForest')




<br />

*__Instructions__*

<br />

__You first install these packages:__

install.packages('devtools')

install.packages('forecast')

install.packages('stringr')

<br />

__Then you load devtools:__

library(devtools)

<br />

__Then you install this package:__

install_github("snarf-snarf/karma")

<br />

__Then you load all packages:__
```R
library(karma)

library(forecast)

library(stringr)
```

<br />


__Examples:__


Auto-select and fit a SARIMA model using auto.karma:

```R
sfit <- auto.karma( mdeaths )   # train SARIMA/ARIMA model
```

Retrain selected model on training-set and make prediction on unseen data (test-set) using default splitting:

```R
karma.cv( sfit )   # will plot predicted vs. actual test-set data and print out-of-sample MAPE on stdout
```

Forecast future periods:

```R
plot(forecast( sfit ))    # plot predicted data
```

<br/>

__Or you can follow the examples on this script:__

https://github.com/snarf-snarf/karma/blob/master/karma_demo.r

Use help() on any function to see a more detailed documentation.
