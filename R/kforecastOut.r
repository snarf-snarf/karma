#' Evaluate autoregressive moving average model (ARMA) model accuracy on specified forecast window (can be either in-sample or out-of-sample data). 
#'
#' @param yt input time series vector
#' @param arima_obj input model object (output of arma() or Arima())
#' @param terms input AR, MA terms list
#' @param h input forecast window size integer
#' @param lib input library name string used to create model object ('tseries' or 'forecast'). Defaults to 'tseries'.
#' @return Forecasted time series vector.
#' @seealso \code{\link{tseries}}, \code{\link{forecast}}
#' @export
#' @examples
#' library(tseries)
#' #Fit ARMA model:
#' dyt = diff(WWWusage)
#' arma1 = arma( x = dyt, lag = list(ar = c(1,2,3) ))
#' yt_hat = forecastOut( dyt[1:70], arma1, terms=list(ar = c(1,2,3)), h=19 )
#' #Plot predicted series: 
#' plot(dyt[1:99], type="l", col="black")
#' points(yt_hat, col="red", type="l") 

cforecastOut <- function( arima_obj, yt, h, constant = "auto" ){
  
  #Get stationarity flag:
  if(constant=="auto"){
    stationarity = karma.adf(yt, modeltype="drift", dw=F, stdout=F)$stationarity == T
  }
  
  #Get AR/MA terms:
  terms = c()
  terms$ar = c()
  terms$ma = c()
  for(i in 1:length(arima_obj$coef)){
    term_str = names(arima_obj$coef)[i]
    term_name = str_sub(term_str, 1, 2)
    term_value = str_sub(term_str, 3, -1)
    if(term_name=='ar'){
      terms$ar = c(terms$ar, as.numeric(term_value))
    }else if(term_name=='ma'){
      terms$ma = c(terms$ma, as.numeric(term_value))
    }
  }
  
  max_term = max(unlist(terms))
  yt_tmp = c(yt[(length(yt)-max(unlist(terms))+1):length(yt)], numeric(h)) #create new series vector with past value according to k and future values according to h.
  eps = residuals(arima_obj) #e_t
  eps[is.na(eps)] = 0  
  eps_tmp = c(eps[(length(eps)-max(unlist(terms))+1):length(eps)], numeric(h))
  eps_tmp[is.na(eps_tmp)] = 0  
  eps_total = c(eps[1:length(eps)], numeric(h))
  eps_total[is.na(eps_tmp)] = 0    
  yt_hat = numeric(h)
  beta = coefficients(arima_obj)
  
  for(t in 1:(length(yt_hat)+max_term)){
    
    i = t-max_term
    if(i>0){
      
      #AR terms:
      if(sum(is.element(names(terms), "ar"))){
        for(p in 1:length(terms$ar)){
          k = terms$ar[p]
          yt_hat[i] = yt_hat[i] + beta[p]*yt_tmp[(max_term+i)-k]   #y_(t-k)
        }
      }else{
        p = 0
      }
      
      #MA terms:
      if(sum(is.element(names(terms), "ma"))){
        for(q in 1:length(terms$ma)){
          k = terms$ma[q]
          yt_hat[i] = yt_hat[i] + beta[p+q]*eps_tmp[(max_term+i)-k]   #e_(t-k) 
        }      
      }
      #yt_hat[t] = yt_hat[t] + eps[t] #e_(t)   #<- the error doesn't go into the prediction
      
      #Constant term: (add only if the series is stationary as per Hyndman: http://robjhyndman.com/hyndsight/arimaconstants/)
      if(constant == "yes"){
        yt_hat[t] = yt_hat[t] + beta[length(beta)]
      }else if(constant == "no"){
        #do nothing
      }else if(constant == "auto"){
        if( stationarity == T ){
          yt_hat[t] = yt_hat[t] + beta[length(beta)]
        }
      }else{
        #...
      }
      
      
      #Copy series:
      if((t+max_term) <= length(yt_tmp)){
        yt_tmp[i+max_term] = yt_hat[i]   #copy forecasted value in series vector (yt_tmp)
        yt_delme = c(yt, yt_tmp[1:(i-1+max_term)])
        eps_total[i+length(yt)] = residuals( arma( x=yt_delme, lag=list(ar=c(1, 7, 13), ma=c(1)) )  )[length(yt_delme)]
        eps_tmp[i+max_term] = eps_total[i+length(yt)]
      }    
    }
  }
  
  return(yt_hat)
}


