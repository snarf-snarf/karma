
#--------------------------------------------------------
# API for automated time series ML
#--------------------------------------------------------

#---------------------------------------
#Timestamp to features:
#---------------------------------------

#Convert timestamp character vector to features data frame (used on timestamp character vector object):
karma.time2seconds <- function(date_vec){   #Works for date format: "2015-11-26 00:00:00.000"
  
  time_df = data.frame(timestamp = numeric(length(date_vec)))
  time_df$timestamp = as.character(date_vec)
  
  time_df$seconds = as.numeric( str_split_fixed(time_df$timestamp, ":", 3)[,3] ) #seconds
  time_df$minutes = as.numeric( str_split_fixed(time_df$timestamp, ":", 3)[,2] )  #minutes
  time_df$hours = as.numeric( str_split_fixed( str_split_fixed(time_df$timestamp, " ", 3)[,2], ":", 2)[,1] ) #hours
  time_df$year = as.numeric( str_split_fixed(time_df$timestamp, "-", 3)[,1] ) #year
  time_df$month = as.numeric( str_split_fixed(time_df$timestamp, "-", 3)[,2] ) #month
  time_df$day = as.numeric( str_split_fixed( str_split_fixed(time_df$timestamp, "-", 3)[,3], " ", 2)[,1] ) #day
  
  time_df$timestamp_secs = time_df$seconds + time_df$minutes*60 + time_df$hours*60*60 + time_df$day*24*60*60 + time_df$month*30*24*60*60
  
  return(time_df)
  
}  #usage: t = karma.time2seconds(timestamp_vec)  #NB: timestamp_vec[1] has to be in format: "2008-07-31 00:00:00.000"



#Create X matrix of features from time-like structure (used on "ts" object):
karma.time2features <- function(y){
  
  if(class(y) != "ts"){
    stop('TSML alert in karma.time2features: Time series argument was not passed as a "ts" object.')
  }
  
  freq = frequency(y)
  x = data.frame( cycle = numeric(length(y)), season = numeric(length(y)) )  #e.g. for monthly data: cyclicity is 1 year, seasonality is 12 month 
  f_vec = rep(1:freq, nrow(x)/freq)
  x$season[1:length(f_vec)] = f_vec
  x$season[x$season==0] = f_vec[1:sum(x$season==0)]   #index of season within a year
  
  c_vec = numeric(length(f_vec))
  spy = round(12/freq) #seasons per year 
  c = 1
  for( i in seq(1, length(f_vec), spy+1) ){
    c_vec[i:(i+spy)] = c
    c = c+1
  }
  x$cycle[1:length(c_vec)] = c_vec
  x$cycle[x$cycle==0] = c   #index of season within a year
  
  return( x )
  
} #usage: karma.time2features(JohnsonJohnson)


#Create X matrix of features from AR terms:
karma.ARset <- function(y, ar_terms = NULL){
  
  x = stats::lag(y, 1)
  if(ar_terms > 1){
    for(j in 2:ar_terms){
      x = cbind(x, stats::lag(y, j))
    }
  }
  
  for(i in 1:dim(x)[1]){
    if( sum(is.na(x[i,])) ){
      x[i,][is.na(x[i,])] = mean(x[i,][!is.na(x[i,])]) #replace NA values in row with the mean of non-NA values
    }
  }
  
  x = x[(dim(x)[1]-length(y)+1):dim(x)[1],]
  x = data.frame(x)
  xnames = paste(rep("ar",dim(x)[2]), 1:dim(x)[2], sep = "")
  names(x) = xnames
  
  return( x )
}



#-----------------------------
# ARRF wrappers:
#-----------------------------

#TS random forest:
tsml.rf <- function(y, ar_terms = NULL, seasonality = F, xreg = NULL, plot = T, stdout = F){
  
  if( seasonality == T ){  #<- not yet implemented in tsml.forecast and tsml.cv
    x = cbind( karma.ARset(y, ar_terms), karma.time2features(y) )
  }else{
    x = karma.ARset(y, ar_terms)  
  }
  
  rf <- randomForest(x, y, prox=TRUE)
  
  rf$fitted = predict(rf)
  rf$ar_terms = ar_terms
  rf$x = y
  rf$X = x
  
  class(rf) = c("tsml.rf", class(rf))
  return(rf)
} #usage: rf0 = tsml.rf( y = mdeaths, ar_terms = 10 )





#function forecast:

tsml.forecast <- function( fit_tmp, h = 12, plot = F){
  
  ar_max = fit_tmp$ar_terms
  
  #forecast:
  ar_terms = 1:ar_max
  X_test = data.frame()
  yt = fit_tmp$x
  ar_vec = c()
  y_hat = c()
  
  while(length(y_hat) <= h){
    for(k in ar_terms){
      ar_vec = c(ar_vec, yt[length(yt)-(k-1)])
    }
    X_test = rbind(X_test, ar_vec)
    names(X_test) = paste(rep("ar",dim(X_test)[2]), 1:dim(X_test)[2], sep = "")
    
    y_hat_tmp = predict( fit_tmp, X_test )
    
    ar_vec = c(ar_vec[(length(y_hat_tmp)+1):length(ar_vec)], y_hat_tmp)
    
    y_hat = c(y_hat, y_hat_tmp)
  }
  y_hat = y_hat[1:h]
  
  if(plot == T){
    #plot(as.numeric(c(y_train, y_hat)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
    #points(as.numeric(y), type="l", col="red")     
    plot(c(yt, y_hat), type = "l", xlab = "time", ylab = "y")
    points((length(yt) + 1):(length(yt) + h), y_hat, col = "blue", type = "l", pch = 22, lwd = 2)    
  }
  return(y_hat)
}#usage: rf0 = tsml.rf( y = mdeaths, ar_terms = 10 ); tsml.forecast(rf0, plot = T)




tsml.cv <- function( rf0, test_pct = 20, test_type = "percentage", log = F, metric = "MAPE", xreg = NULL, plot = T, cv = "out", stdout = F ){
  
  y = rf0$x
  ar_max = rf0$ar_terms
  
  if( cv == "in" ){
    if(log == T){
      y_hat = exp( rf0$fitted )
      y = exp(y)
    }else if (log == F){    
      y_hat = fitted( rf0 )  #'forecast' package
    }
    mape_in = karma.validate(y[1:length(y_hat)], y_hat, metric = metric) #mmetric(y_hat, y[1:length(y_hat)], "MAPE") 
    
    #Plots:
    if(plot == T){
      plot(as.numeric(y_hat), type='l', xlab = 'time', ylab = 'y', main="In-sample forecast")
      points(as.numeric(y), type='l', col='red')
    }
    
    return(mape_in)
  }
  
  if( cv == "out" ){
    
    if(test_type == "percentage" ){
      test_size = round(length(y)*test_pct/100)  #size as percentage of the length of the series
    }else if(test_type == "window" ){ 
      test_size = test_pct  #fixed window size (e.g. in months)
    }
    
    train_size = length(y)-test_size
    if(class(y) == "numeric"){
      y_train = y[1:train_size]
    }else if(class(y) == "ts"){  #Arima( y = JohnsonJohnson, order = karma0$model_terms, seasonal = karma0$seasonal_terms)
      y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    }    
    
    #train_size = length(y)-test_size
    #y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    
    #train:
    fit_tmp = tsml.rf( y = y_train, ar_terms = ar_max )
    
    #test:
    #y_train = fit_tmp$x
    
    #CV in-sample:
    y_hat = tsml.forecast(fit_tmp, h = test_size, plot = F)
    
    if( plot == T ){
      plot(as.numeric(c(y_train, y_hat)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
      points(as.numeric(y), type="l", col="red")   
    }
    
    mape_out = karma.validate(y[(train_size + 1):(train_size + test_size)], y_hat, metric = metric) #y[train_size:(train_size+test_size-1)]
    return(mape_out)
  }
  
} #usage: rf0 = tsml.rf( y = mdeaths, ar_terms = 10 ); tsml.cv(rf0); tsml.forecast(rf0, h = 12, plot = T)

# #-------------------------------------------
# #ARRF Demo:
# #-------------------------------------------
# library(forecast);library(stringr);library(karma);library(randomForest)
# yt = diff(log(JohnsonJohnson))
# 
# #Train model:
# rf0 = tsml.rf( y = yt, ar_terms = 15 )
# 
# #Forecast:
# tsml.forecast(rf0, h = 12, plot = T)
# 
# #Test 70-30:
# tsml.cv(rf0, test_pct = 12, test_type = "window")



#-------------------------------------
#nnfor/TStools wrappers
#-------------------------------------
##nnfor/TStools: 
#library(nnfor)  #<- prwhn library(TStools)
#mlp.fit <- nnfor::mlp(y = mdeaths)
#mlp.cv(mlp.fit, test_type = "window", test_pct = 12, keep_structure = T) #<-mine
#mlp.frc <- forecast(mlp.fit,h=12); 
#plot(mlp.frc)
library(nnfor)
mlp.cv <- function( mfit, test_type = c("window", "percentage", "auto"), test_pct = 12, xreg = c(), metric = "MAPE", keep_structure = T ){
  
  y = mfit$y
  test_size = test_pct
  train_size = length(y)-test_size
  y_train = y[1:train_size]
  y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
  
  #Fit train set:
  if( keep_structure == F ){
    fit_tmp = nnfor::mlp( y = y_train, xreg = xreg )
  }else{
    fit_tmp = nnfor::mlp( y = y_train, hd = mfit$hd, difforder = mfit$difforder, lags = mfit$lags,  xreg = xreg )
  }
  
  #Predict test set:
  fc = forecast(fit_tmp, h = test_pct)  #plot(fc)
  y_hat2 = fc$mean
  
  #Plot:
  plot(as.numeric(c(y_train, y_hat2)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
  points(as.numeric(y), type="l", col="red")
  
  #Get test MAPE:
  mape_out = karma.validate(y[(train_size+1):(train_size+test_size)], y_hat2, metric = metric)
  
  return(mape_out)
  
} #usage: mlp.fit <- nnfor::mlp(y = mdeaths); mlp.cv(mlp.fit, test_type = "window", test_pct = 12, keep_structure = T); plot(forecast(mlp.fit,h=12)); 






#-------------------------------------
#Prophet wrappers
#-------------------------------------

library(zoo)
library(fpp2)

library(prophet)

prophet.fit <- function(y){
  
  #Prepare input data:
  dates = as.Date.yearmon(time(y))   #dates
  df0 = data.frame(ds = dates, y = y)
  #Train:
  pfit = prophet(df0)
  fitted = prophet.forecast(pfit, h = 1, plot = F)$yhat
  pfit$fitted = fitted[1:(length(fitted)-1)]
  pfit$x = y
  
  return(pfit)
  
} #usage: pfit1 = prophet.fit(AirPassengers)

prophet.forecast <- function(pfit, h = 12, plot = T){
  
  future0 <- make_future_dataframe(pfit, periods = h)
  fcast0 <- predict(pfit, future0)
  y = pfit$x
  yf = fcast0$yhat[(length(y)+1):(length(fcast0$yhat))]
  yl = fcast0$yhat_lower[(length(y)+1):(length(fcast0$yhat_lower))]
  yu = fcast0$yhat_upper[(length(y)+1):(length(fcast0$yhat_upper))]
  
  #plot(as.numeric(fcast0$yhat), type = 'l')
  #points(as.numeric(pfit$x), type = 'l', col = 'red')
  
  if(plot == T){
    plot(as.numeric(y), type = "l", xlab = "time", ylab = "y", main = "Prophet forecast", xlim = c(0,length(y)+h), ylim = c(min(c(y,yf)), max(c(y,yf))) )
    points((length(y) + 1):(length(y) + h), yf, col = "blue", type = "l", pch = 22, lwd = 2)
    points((length(y) + 1):(length(y) + h), yl, col = "green", type = "l", pch = 22, lty = 2)
    points((length(y) + 1):(length(y) + h), yu, col = "green", type = "l", pch = 22, lty = 2)
  }
  
  return(fcast0) #<- uncommenting this line will cancel out the plot
  
} #usage: pfit1 = prophet.fit(AirPassengers); fcast1 = prophet.forecast(pfit1); plot(pfit1, fcast1)



prophet.cv <- function( pfit0, test_pct = 20, test_type = "percentage", log = F, metric = "MAPE", xreg = NULL, plot = T, cv = "out", stdout = F ){
  
  y = pfit0$x
  
  if( cv == "in" ){
    if(log == T){
      y_hat = exp( pfit0$fitted )
      y = exp(y)
    }else if (log == F){    
      y_hat = pfit0$fitted
    }
    mape_in = karma.validate(y[1:length(y_hat)], y_hat, metric = metric) #mmetric(y_hat, y[1:length(y_hat)], "MAPE") 
    
    #Plots:
    if(plot == T){
      plot(as.numeric(y_hat), type='l', xlab = 'time', ylab = 'y', main="In-sample forecast")
      points(as.numeric(y), type='l', col='red')
    }
    
    return(mape_in)
  }
  
  if( cv == "out" ){
    
    if(test_type == "percentage" ){
      test_size = round(length(y)*test_pct/100)  #size as percentage of the length of the series
    }else if(test_type == "window" ){ 
      test_size = test_pct  #fixed window size (e.g. in months)
    }
    
    train_size = length(y)-test_size
    if(class(y) == "numeric"){
      y_train = y[1:train_size]
    }else if(class(y) == "ts"){  #Arima( y = JohnsonJohnson, order = karma0$model_terms, seasonal = karma0$seasonal_terms)
      y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    }    
    
    #train_size = length(y)-test_size
    #y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    
    #train:
    fit_tmp = prophet.fit( y = y_train )
    
    #test:
    #y_train = fit_tmp$x
    
    #CV in-sample:
    tmp = prophet.forecast(fit_tmp, h = test_size)$yhat
    y_hat = tmp[train_size:(train_size+test_size-1)]
    
    if( plot == T ){
      plot(as.numeric(c(y_train, y_hat)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
      points(as.numeric(y), type="l", col="red")   
    }
    
    mape_out = karma.validate(y[(train_size + 1):(train_size + test_size)], y_hat, metric = metric)
    return(mape_out)
  }
  
} #usage: pfit1 = prophet.fit(AirPassengers); prophet.cv(pfit1, cv = "out", test_type = "window", test_pct = 12); prophet.cv(pfit1,cv='in')

# Test Prophet wrappers:
# yt = AirPassengers
# pfit1 = prophet.fit(yt)
# prophet.cv(pfit1, cv = "in")
# prophet.cv(pfit1, cv = "out", test_type = "window", test_pct = 12)
# fcast1 = prophet.forecast(pfit1, h = 12)
# plot(pfit1, fcast1)



#---------------------------------------------------------------------
# Holt-Winters 
#---------------------------------------------------------------------

hw.forecast <- function(fit0, h = 12, plot = T){
  
  y = fit0$x
  yf <- predict(fit0, h)
  
  if(plot == T){
    plot(as.numeric(y), type = "l", xlab = "time", ylab = "y", main = "HoltWinters forecast", xlim = c(0,length(y)+h), ylim = c(min(c(y,yf)), max(c(y,yf))) )
    points((length(y) + 1):(length(y) + h), yf, col = "blue", type = "l", pch = 22, lwd = 2)
  }
  
  return(yf) #<- uncommenting this line will cancel out the plot
  
}

hw.cv <- function( fit0, seasonal = "mult", test_pct = 20, test_type = "percentage", log = F, metric = "MAPE", xreg = NULL, plot = T, cv = "out", stdout = F ){
  
  y = fit0$x
  
  if(class(fit0) == "HoltWinters"){
    #tsml.fit = HoltWinters
    tsml.predict = predict
    fit0$fitted0 = fit0$fitted
    fit0$fitted = fit0$fitted[,1]
  }
  
  if( cv == "in" ){
    if(log == T){
      y_hat = exp( fit0$fitted )
      y = exp(y)
    }else if (log == F){    
      y_hat = fit0$fitted
    }
    mape_in = karma.validate(y[1:length(y_hat)], y_hat, metric = metric) #mmetric(y_hat, y[1:length(y_hat)], "MAPE") 
    
    #Plots:
    if(plot == T){
      plot(as.numeric(y_hat), type='l', xlab = 'time', ylab = 'y', main="In-sample forecast")
      points(as.numeric(y), type='l', col='red')
    }
    
    return(mape_in)
  } #usage: m <- HoltWinters(mdeaths, seasonal = "mult"); hw.forecast(m, 12)
  
  if( cv == "out" ){
    
    if(test_type == "percentage" ){
      test_size = round(length(y)*test_pct/100)  #size as percentage of the length of the series
    }else if(test_type == "window" ){ 
      test_size = test_pct  #fixed window size (e.g. in months)
    }
    
    train_size = length(y)-test_size
    if(class(y) == "numeric"){
      y_train = y[1:train_size]
    }else if(class(y) == "ts"){  #Arima( y = JohnsonJohnson, order = karma0$model_terms, seasonal = karma0$seasonal_terms)
      y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    }    
    
    #train_size = length(y)-test_size
    #y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    
    #train:
    fit_tmp = HoltWinters( y_train, seasonal = seasonal )
    
    #test:
    #y_train = fit_tmp$x
    
    #CV in-sample:
    y_hat = tsml.predict(fit_tmp, test_size)
    
    if( plot == T ){
      plot(as.numeric(c(y_train, y_hat)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
      points(as.numeric(y), type="l", col="red")   
    }
    
    mape_out = karma.validate(y[(train_size + 1):(train_size + test_size)], y_hat, metric = metric)
    return(mape_out)
  }
  
} #usage: m <- HoltWinters(mdeaths, seasonal = "mult"); hw.cv(m, cv='out', seasonal = "mult")

# # Test Holt-Winters wrappers:
# y0 = mdeaths
# m <- HoltWinters(y0, seasonal = "mult")
# plot(m) #plot(fitted(m))
# hw.cv(m, cv='out', seasonal = "mult")
# hw.forecast(m, 12)


#---------------------------------------------------------------------
# Bayesian structural time series
#---------------------------------------------------------------------

library(bsts)     # load the bsts package


bsts.fit <- function(y, freq = c(), niter = 1000){
  
  if(is.null(freq)){
    freq = frequency(y)
  }
  
  #Train:  
  ss <- AddLocalLinearTrend(list(), y)
  ss <- AddSeasonal(ss, y, nseasons = freq)
  bfit <- bsts(y, state.specification = ss, niter = niter);
  bfit$x = y;
  bfit$niter = niter
  bfit$freq = freq
  #bfit$fitted = predict(bfit) #plot(pred1, plot.original = length(y))
  
  class(bfit) = c("bsts.fit", class(bfit))
  return(bfit)
} #usage: bfit = bsts.fit(JohnsonJohnson); plot(bfit); plot(bfit, "components")

bsts.forecast <- function(bfit, h = 12, plot = T){
  
  pred1 <- predict(bfit, horizon = h);
  
  if(plot == T){
    plot(pred1, plot.original = 156);
  }
  
  class(pred1) = c("bsts.forecast", class(pred1))
  return(pred1) #<- uncommenting this line will cancel out the plot
  
} #bfit = bsts.fit(JohnsonJohnson); bsts.forecast(bfit)



bsts.cv <- function( bfit0, test_pct = 20, test_type = "percentage", log = F, metric = "MAPE", xreg = NULL, plot = T, cv = "out", stdout = F ){
  
  y = bfit0$x
  
  if( cv == "in" ){
    print("No in-sample validation supported in BSTS; switching to out-of-sample.")
    cv = "out"
  }
  
  if( cv == "out" ){
    
    if(test_type == "percentage" ){
      test_size = round(length(y)*test_pct/100)  #size as percentage of the length of the series
    }else if(test_type == "window" ){ 
      test_size = test_pct  #fixed window size (e.g. in months)
    }
    
    train_size = length(y)-test_size
    if(class(y) == "numeric"){
      y_train = y[1:train_size]
    }else if(class(y) == "ts"){  #Arima( y = JohnsonJohnson, order = karma0$model_terms, seasonal = karma0$seasonal_terms)
      y_train = ts( y[1:train_size], start = time(y)[1], end = time(y)[train_size], frequency = frequency(y) )
    }    
    
    #--------------------------------------------------------------------
    #train:
    fit_tmp = bsts.fit( y = y_train, freq = bfit0$freq, niter = bfit0$niter )
    
    #test on held out sample:
    y_hat = bsts.forecast(fit_tmp, h = test_size)$mean
    #--------------------------------------------------------------------
    
    
    if( plot == T ){
      plot(as.numeric(c(y_train, y_hat)), type='l', xlab = 'time', ylab = 'y', main="Out-of-sample forecast")   #plot( forecast( fit_tmp, h=test_size ) )
      points(as.numeric(y), type="l", col="red")   
    }
    
    mape_out = karma.validate(y[(train_size + 1):(train_size + test_size)], y_hat, metric = metric)
    return(mape_out)
  }
  
} #usage: bfit = bsts.fit(mdeaths); bsts.cv(bfit, cv = "out", test_type = "window", test_pct = 12)

# Test bsts wrappers:
# yt = JohnsonJohnson
# pfit1 = bsts.fit(yt)
# bsts.cv(pfit1, cv = "out", test_type = "window", test_pct = 12)   #bsts.cv(pfit1, cv = "in")
# fcast1 = bsts.forecast(pfit1, h = 12)
# plot(pfit1, fcast1)




