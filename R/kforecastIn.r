#' Evaluate autoregressive moving average model (ARMA) model accuracy on in-sample data.
#'
#' @param yt input time series vector
#' @param arima_obj input model object (output of arma() or Arima())
#' @param terms input AR, MA terms list
#' @param lib input library name string used to create model object ('tseries' or 'forecast'). Defaults to 'tseries'.
#' @return Forecasted time series vector.
#' @seealso \code{\link{tseries}}, \code{\link{forecast}}
#' @export
#' @examples
#' library(tseries)
#' #Fit ARMA model:
#' dyt = diff(WWWusage)
#' arma1 = arma( x = dyt, lag = list(ar = c(1,2,3) ))
#' yt_hat = forecastIn( dyt, arma1, terms=list(ar = c(1,2,3)) )
#' #Plot predicted series:
#' plot(yt_hat, col="red", type="l") 
#' points(dyt, type="l", col="black")

#require(stringr)

cforecastIn <- function( arima_obj, yt, constant = "auto" ){
  
  ##Get model coefficient Standard Error:
  #se_vec = sqrt(diag(arima_obj$var.coef))
  #pval_vec = ( 1 - pnorm( abs(arima_obj$coef)/se_vec ) )*2
  
  #Get series:
  if(missing(yt)){
    yt = arima_obj$x
  }
  
  #Get stationarity flag:
  if(constant=="auto"){
    stationarity = karma.adf(yt, modeltype="drift", dw=F, stdout=F)$stationarity == T
  }  
  
  #Get AR/MA terms:
  terms = c()
  terms$ar = c()
  terms$ma = c()
  for(i in 1:length(arima_obj$coef)){
    term_str = names(arima_obj$coef)[i]
    term_name = str_sub(term_str, 1, 2)
    term_value = str_sub(term_str, 3, -1)
    if(term_name=='ar'){
      terms$ar = c(terms$ar, as.numeric(term_value))
    }else if(term_name=='ma'){
      terms$ma = c(terms$ma, as.numeric(term_value))
    }
  }

  #yt_tmp = yt #y_t
  eps = residuals(arima_obj) #e_t
  eps[is.na(eps)] = 0  
  yt_hat = numeric(length(yt))
  beta = coefficients(arima_obj)  
  
  for(t in 1:length(yt)){
    
    #AR terms:
    if(sum(is.element(names(terms), "ar"))){
      for(p in 1:length(terms$ar)){
        k = terms$ar[p]
        if(t-k > 0){
          yt_hat[t] = yt_hat[t] + beta[p]*yt[t-k]   #y_(t-k)
        }      
      }
    }else{
      p = 0
    }
    
    #MA terms:
    if(sum(is.element(names(terms), "ma"))){
      for(q in 1:length(terms$ma)){
        k = terms$ma[q]
        if(t-k > 0){
          yt_hat[t] = yt_hat[t] + beta[p+q]*eps[t-k]   #e_(t-k)
        }        
      }
    }
    #yt_hat[t] = yt_hat[t] + eps[t] #e_(t)   #<- the error doesn't go into the model, it's the stochastic factor
    
    #Constant term: (add only if the series is stationary as per Hyndman: http://robjhyndman.com/hyndsight/arimaconstants/)
    if(constant == "yes"){
      yt_hat[t] = yt_hat[t] + beta[length(beta)]
    }else if(constant == "no"){
      #do nothing
    }else if(constant == "auto"){
      if( stationarity == T ){
        yt_hat[t] = yt_hat[t] + beta[length(beta)]
      }
    }else{
      #...
    }
  }
  
  return(as.ts(yt_hat))
}


